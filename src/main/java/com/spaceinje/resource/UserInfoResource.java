/**
 * 
 */
package com.spaceinje.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.bean.UserInfoBean;
import com.spaceinje.constants.MasterConstants;
import com.spaceinje.service.UserInfoService;
import com.spaceinje.util.ResponseCachet;
import com.spaceinje.validator.RequestValidator;

import brave.Tracer;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Lakshmmi Kiran
 * @implNote Admin controller which serves admin requests
 * @version 1.0
 */
@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/v1/master/user")
public class UserInfoResource {

	private static final Logger log = LoggerFactory.getLogger(UserInfoResource.class);

	@Autowired
	private UserInfoService adminUserService;

	@Autowired
	private RequestValidator requestValidator;
	
	@Autowired
	private MasterConstants constants;
	
	@Autowired
	private Tracer tracer;

	/**
	 * service to save user
	 * 
	 * @param bean - UserInfoBean
	 * 
	 * @return {@value ResponseEntity}
	 */
	@ApiResponse(content = @Content(schema = @Schema(implementation = UserInfoBean.class)))
	@RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseCachet> saveUser(@RequestBody UserInfoBean bean) {
		if (log.isDebugEnabled())
			log.debug("Entering saveUser service");
		log.info("validating the request bean", bean);

		ResponseCachet cachet = requestValidator.isUserRequestValid(bean, constants.getService_type_save());
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
					.replace("LazySpan(", ""));
			if (log.isDebugEnabled())
				log.debug("Exiting saveUser service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.info("calling Admin user service");
		cachet = adminUserService.saveUserInfo(bean);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
					.replace("LazySpan(", ""));
			if (log.isDebugEnabled())
				log.debug("Exiting saveUser service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (log.isDebugEnabled())
			log.debug("Exiting saveUser service");
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to update user
	 * 
	 * @param bean - UserInfoBean
	 * 
	 * @return {@value ResponseEntity}
	 */
	@RequestMapping(method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseCachet> updateUser(@RequestBody UserInfoBean bean) {
		if (log.isDebugEnabled())
			log.debug("Entering updateUser service");
		
		log.info("validating the request bean");
		ResponseCachet cachet = requestValidator.isUserRequestValid(bean, constants.getService_type_update());
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
					.replace("LazySpan(", ""));
			if (log.isDebugEnabled())
				log.debug("Exiting updateUser service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.info("calling Admin user service");
		cachet = adminUserService.updateUserInfo(bean);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
					.replace("LazySpan(", ""));
			if (log.isDebugEnabled())
				log.debug("Exiting updateUser service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (log.isDebugEnabled())
			log.debug("Exiting updateUser service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to delete user
	 * 
	 * @param id - id
	 * 
	 * @return {@value ResponseEntity}
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseCachet> deleteUser(
			@RequestParam(name = "id", required = true) String id) {
		if (log.isDebugEnabled())
			log.debug("Entering deleteUser service");
		
		log.info("calling user service");
		ResponseCachet cachet = adminUserService.deleteUserInfo(id);
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			if (log.isDebugEnabled())
				log.debug("Exiting deleteUser service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (log.isDebugEnabled())
			log.debug("Exiting deleteUser service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
