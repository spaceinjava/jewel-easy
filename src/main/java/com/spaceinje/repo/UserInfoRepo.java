/**
 * 
 */
package com.spaceinje.repo;

import com.spaceinje.model.UserInfo;

/**
 * @author Lakshmi Kiran
 * @implNote Repository for admin user
 * @version 1.0
 */
public interface UserInfoRepo {

	public UserInfo findByEmailOrMobile(String email, String mobile, boolean is_tenant);
	
	public UserInfo save(UserInfo info);
	
	public UserInfo findById(String id);
	
	public void delete(UserInfo info);
	
	public UserInfo findByAdminCode(String admin_code);
	
	public UserInfo findByTenantCode(String tenant_code);
}
