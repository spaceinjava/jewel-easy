/**
 * 
 */
package com.spaceinje.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.spaceinje.model.SessionTracker;

/**
 * @author Lakshmi Kiran
 * @implNote repository to interact with session tracker collection in mongo
 * @version 1.0
 */
@Repository
public interface SessionTrackerRepo extends MongoRepository<SessionTracker, String>{
	
	@Query("{code : ?0}")
	SessionTracker findByCode(String code);
	
}
