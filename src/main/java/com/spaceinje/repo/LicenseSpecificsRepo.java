/**
 * 
 */
package com.spaceinje.repo;

import com.spaceinje.model.LicenseSpecifics;

/**
 * @author Lakshmi Kiran
 *
 */
public interface LicenseSpecificsRepo {
	
	public LicenseSpecifics save(LicenseSpecifics specifics);
	
	public LicenseSpecifics findByTenantCode(String tenant_code);
	
	public void delete(String id);
	
	public boolean existsByTenantCode(String tenant_code);
}
