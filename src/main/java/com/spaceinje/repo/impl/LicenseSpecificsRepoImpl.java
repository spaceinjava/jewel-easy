/**
 * 
 */
package com.spaceinje.repo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.spaceinje.model.LicenseSpecifics;
import com.spaceinje.repo.LicenseSpecificsRepo;

/**
 * @author Lakshmi Kiran
 *
 */
@Repository("licenseSpecificsRepo")
public class LicenseSpecificsRepoImpl implements LicenseSpecificsRepo {

	private final MongoTemplate mongoTemplate;

	@Autowired
	public LicenseSpecificsRepoImpl(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	
	@Override
	public LicenseSpecifics save(LicenseSpecifics specifics) {
		return mongoTemplate.save(specifics);
	}

	@Override
	public LicenseSpecifics findByTenantCode(String tenant_code) {
		Query query = new Query();
		query.addCriteria(Criteria.where("tenant_code").is(tenant_code));
		return mongoTemplate.findOne(query, LicenseSpecifics.class);
	}

	@Override
	public void delete(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		mongoTemplate.remove(query, LicenseSpecifics.class);
		
	}

	@Override
	public boolean existsByTenantCode(String tenant_code) {
		Query query = new Query();
		query.addCriteria(Criteria.where("tenant_code").is(tenant_code));
		return mongoTemplate.exists(query, LicenseSpecifics.class);
	}

}
