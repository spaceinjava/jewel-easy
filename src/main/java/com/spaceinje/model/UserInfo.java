/**
 * 
 */
package com.spaceinje.model;

import java.io.Serializable;

import org.bson.codecs.pojo.annotations.BsonProperty;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.spaceinje.model.base.AuditEntity;

/**
 * @author Lakshmi Kiran
 * @implNote Back end admin or super admin who can perform operations on tenant,
 *           master and inventory transactions
 * @version 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo extends AuditEntity implements Serializable {

	/**
	 * {@value serialVersionUID}
	 */
	private static final long serialVersionUID = 4113432245845931625L;

	@BsonProperty("_id")
	@Id
	private String id;
	
	@JsonInclude(Include.NON_NULL)
	private boolean is_tenant;
	
	@JsonInclude(Include.NON_NULL)
	private String admin_code;
	
	@JsonInclude(Include.NON_NULL)
	private String tenant_code;
	
	@JsonInclude(Include.NON_NULL)
	private String emp_code;

	@JsonInclude(Include.NON_NULL)
	private String first_name;

	@JsonInclude(Include.NON_NULL)
	private String middle_name;

	@JsonInclude(Include.NON_NULL)
	private String last_name;

	@JsonInclude(Include.NON_NULL)
	private String email;

	@JsonInclude(Include.NON_NULL)
	private String mobile;

	@JsonInclude(Include.NON_NULL)
	private String password;
	
	@JsonInclude(Include.NON_NULL)
	private String profile_picture;

	@JsonInclude(Include.NON_NULL)
	private Integer login_attempts;
	
	@JsonInclude(Include.NON_NULL)
	private String login_status;
	
	@JsonInclude(Include.NON_NULL)
	private String hash;

	@JsonInclude(Include.NON_NULL)
	private String secret;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isIs_tenant() {
		return is_tenant;
	}

	public void setIs_tenant(boolean is_tenant) {
		this.is_tenant = is_tenant;
	}

	public String getAdmin_code() {
		return admin_code;
	}

	public void setAdmin_code(String admin_code) {
		this.admin_code = admin_code;
	}

	public String getTenant_code() {
		return tenant_code;
	}

	public void setTenant_code(String tenant_code) {
		this.tenant_code = tenant_code;
	}

	public String getEmp_code() {
		return emp_code;
	}

	public void setEmp_code(String emp_code) {
		this.emp_code = emp_code;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	public Integer getLogin_attempts() {
		return login_attempts;
	}

	public void setLogin_attempts(Integer login_attempts) {
		this.login_attempts = login_attempts;
	}

	public String getLogin_status() {
		return login_status;
	}

	public void setLogin_status(String login_status) {
		this.login_status = login_status;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Override
	public String toString() {
		return "UserInfo [id=" + id + ", is_tenant=" + is_tenant + ", admin_code=" + admin_code + ", tenant_code="
				+ tenant_code + ", emp_code=" + emp_code + ", first_name=" + first_name + ", middle_name=" + middle_name
				+ ", last_name=" + last_name + ", email=" + email + ", mobile=" + mobile + ", password=" + password
				+ ", profile_picture=" + profile_picture + ", login_attempts=" + login_attempts + ", login_status="
				+ login_status + ", hash=" + hash + ", secret=" + secret + "]";
	}
	
}
