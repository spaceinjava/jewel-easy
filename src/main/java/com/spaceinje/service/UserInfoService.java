/**
 * 
 */
package com.spaceinje.service;

import com.spaceinje.bean.UserInfoBean;
import com.spaceinje.util.ResponseCachet;

/**
 * @author Lakshmi Kiran
 *
 */
@SuppressWarnings("rawtypes")
public interface UserInfoService {

	public ResponseCachet<UserInfoBean> saveUserInfo(UserInfoBean bean);
	
	public ResponseCachet<UserInfoBean> updateUserInfo(UserInfoBean bean);
	
	public ResponseCachet deleteUserInfo(String id);
	
}
