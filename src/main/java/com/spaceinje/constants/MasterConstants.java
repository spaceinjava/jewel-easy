/**
 * 
 */
package com.spaceinje.constants;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Lakshmi Kiran
 * @apiNote contains all the constants which will be used as a single instance
 *          in entire application life cycle
 * @version 1.0
 */
@Component("constants")
public class MasterConstants {

	String ternary = "?";

	String colon = ":";

	String single_slash = "/";

	String double_slash = "//";

	String and = "&";

	String equals = "=";

	@Value("${status.message.success}")
	String status_message_success;

	@Value("${status.message.failure}")
	String status_message_failure;

	@Value("${status.message.exception}")
	String status_message_exception;

	@Value("${email.regex}")
	String email_regex;

	@Value("${mobile.regex}")
	String mobile_regex;

	@Value("${password.regex}")
	String password_regex;

	@Value("${pincode.regex}")
	String pincode_regex;

	@Value("${name.regex}")
	String name_regex;

	@Value("${secret.key.salt.iterations}")
	int secret_key_salt_iterations;

	@Value("${secret.key.salt.algo}")
	String secret_key_salt_algo;

	@Value("${secret.key.salt.algo.instance}")
	String secret_key_salt_algo_instance;

	@Value("${alpha.capital.case}")
	String alpha_capital_case;

	@Value("${alpha.small.case}")
	String alpha_small_case;

	@Value("${special.chars}")
	String special_chars;

	@Value("${natural.numbers}")
	String natural_numbers;

	@Value("${status.response.type.code}")
	String status_response_type_code;

	@Value("${status.response.type.token}")
	String status_response_type_token;

	@Value("${status.response.type.password}")
	String status_response_type_password;

	@Value("${secret.key.left.limit}")
	int secret_key_left_limit;

	@Value("${secret.key.right.limit}")
	int secret_key_right_limit;

	@Value("${secret.key.target.length}")
	int secret_key_target_length;

	@Value("${auth.code.expiry.in.seconds}")
	int auth_code_expiry_in_seconds;

	@Value("${auth.token.expiry.in.seconds}")
	int auth_token_expiry_in_seconds;

	@Value("${admin.service.type.save}")
	int service_type_save;

	@Value("${admin.service.type.update}")
	int service_type_update;

	@Value("${admin.email.subject}")
	String admin_email_subject;

	@Value("${tenant.email.subject}")
	String tenant_email_subject;

	@Value("${license.type.free}")
	String license_type_free;

	@Value("${license.type.paid}")
	String license_type_paid;

	@Value("${license.type.free.days}")
	int license_type_free_days;

	@Value("${license.type.paid.monthly}")
	String license_type_paid_monthly;

	@Value("${license.type.paid.quarterly}")
	String license_type_paid_quarterly;

	@Value("${license.type.paid.annual}")
	String license_type_paid_annual;

	@Value("${license.type.paid.monthly.days}")
	int license_type_paid_monthly_days;

	@Value("${license.type.paid.quarterly.days}")
	int license_type_paid_quarterly_days;

	@Value("${license.type.paid.annual.days}")
	int license_type_paid_annual_days;

	@Value("${end.date.key}")
	String end_date_key;

	@Value("${license.days.key}")
	String license_days_key;

	@Value("${tenant.password.suffix}")
	String tenant_password_suffix;

	@Value("${url.prefix}")
	String db_url_prefix;

	@Value("${db.host}")
	String db_host;

	@Value("${db.port}")
	String db_port;

	@Value("${db.username}")
	String db_username;

	@Value("${db.password}")
	String db_password;

	@Value("${db.driver.class.name}")
	String db_driver_class_name;

	@Value("${url.suffix}")
	String db_url_suffix;

	@Value("#'{${db.query.keys}'.split(',')}")
	List<String> query_keys;

	@Value("${spring.mail.username}")
	String from_email;

	@Value("${env.type}")
	String env_type;

	@Value("${application.gateway}")
	String app_gateway;

	@Value("${validate.params.mapping}")
	String validate_mapping_params;
	
	@Value("${authentication.mapping}")
	String authentication_mapping;
	
	@Value("${service.protocol}")
	String protocol;
	
	@Value("${validate.params.response.type.key}")
	String response_type_key;
	
	@Value("${validate.params.request.hash.key}")
	String hash_key;
	
	@Value("${validate.params.request.secret.key}")
	String secret_key;
	
	@Value("${validate.params.request.code.key}")
	String code_key;
	
	@Value("${validate.params.request.token.key}")
	String token_key;
	
	@Value("${session.validation.mapping}")
	String session_validation_mapping;
	
	@Value("${service.name.key}")
	String service_name_key;
	
	@Value("${spring.security.user.name}")
	String username;
	
	@Value("${spring.security.user.password}")
	String password;
	
	@Value("${password.reset.url}")
	String password_reset_url;
	
	@Value("${max.login.attempts}")
	String max_login_attempts;
	
	@Value("${password.suffix}")
	String password_suffix;
	
	@Value("${forgot.password.subject}")
	String forgot_password_subject;
	
	@Value("${login.status.new}")
	String login_status_new;
	
	@Value("${login.status.activated}")
	String login_status_activated;
	
	@Value("${admin.id.prefix}")
	String admin_id_prefix;
	
	@Value("${tenant.id.prefix}")
	String tenant_id_prefix;
	
	@Value("${id.length}")
	int id_length;
	
	
	/**
	 * @return the admin_id_prefix
	 */
	public String getAdmin_id_prefix() {
		return admin_id_prefix;
	}

	/**
	 * @return the tenant_id_prefix
	 */
	public String getTenant_id_prefix() {
		return tenant_id_prefix;
	}

	/**
	 * @return the id_length
	 */
	public int getId_length() {
		return id_length;
	}

	/**
	 * @return the login_status_new
	 */
	public String getLogin_status_new() {
		return login_status_new;
	}

	/**
	 * @return the login_status_activated
	 */
	public String getLogin_status_activated() {
		return login_status_activated;
	}

	/**
	 * @return the token_key
	 */
	public String getToken_key() {
		return token_key;
	}

	/**
	 * @return the forgot_password_subject
	 */
	public String getForgot_password_subject() {
		return forgot_password_subject;
	}

	/**
	 * @return the password_siffix
	 */
	public String getPassword_suffix() {
		return password_suffix;
	}

	/**
	 * @return the password_reset_url
	 */
	public String getPassword_reset_url() {
		return password_reset_url;
	}

	/**
	 * @return the max_login_attempts
	 */
	public String getMax_login_attempts() {
		return max_login_attempts;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the session_validation_mapping
	 */
	public String getSession_validation_mapping() {
		return session_validation_mapping;
	}

	/**
	 * @return the service_name_key
	 */
	public String getService_name_key() {
		return service_name_key;
	}

	/**
	 * @return the protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * @return the response_type_key
	 */
	public String getResponse_type_key() {
		return response_type_key;
	}

	/**
	 * @return the admin_hash_key
	 */
	public String getHash_key() {
		return hash_key;
	}

	/**
	 * @return the secret_key
	 */
	public String getSecret_key() {
		return secret_key;
	}

	/**
	 * @return the code_key
	 */
	public String getCode_key() {
		return code_key;
	}

	/**
	 * @return the app_gateway
	 */
	public String getApp_gateway() {
		return app_gateway;
	}

	/**
	 * @return the validate_mapping_params
	 */
	public String getValidate_mapping_params() {
		return validate_mapping_params;
	}

	/**
	 * @return the authentication_mapping
	 */
	public String getAuthentication_mapping() {
		return authentication_mapping;
	}

	/**
	 * @return the ternary
	 */
	public String getTernary() {
		return ternary;
	}

	/**
	 * @return the colon
	 */
	public String getColon() {
		return colon;
	}

	/**
	 * @return the single_slash
	 */
	public String getSingle_slash() {
		return single_slash;
	}

	/**
	 * @return the double_slash
	 */
	public String getDouble_slash() {
		return double_slash;
	}

	/**
	 * @return the and
	 */
	public String getAnd() {
		return and;
	}

	/**
	 * @return the equals
	 */
	public String getEquals() {
		return equals;
	}

	/**
	 * @return the env_type
	 */
	public String getEnv_type() {
		return env_type;
	}

	/**
	 * @return the from_email
	 */
	public String getFrom_email() {
		return from_email;
	}

	/**
	 * @return the query_keys
	 */
	public List<String> getQuery_keys() {
		return query_keys;
	}

	/**
	 * @return the db_url_suffix
	 */
	public String getDb_url_suffix() {
		return db_url_suffix;
	}

	/**
	 * @return the db_driver_class_name
	 */
	public String getDb_driver_class_name() {
		return db_driver_class_name;
	}

	/**
	 * @return the db_url_prefix
	 */
	public String getDb_url_prefix() {
		return db_url_prefix;
	}

	/**
	 * @return the db_host
	 */
	public String getDb_host() {
		return db_host;
	}

	/**
	 * @return the db_port
	 */
	public String getDb_port() {
		return db_port;
	}

	/**
	 * @return the db_username
	 */
	public String getDb_username() {
		return db_username;
	}

	/**
	 * @return the db_password
	 */
	public String getDb_password() {
		return db_password;
	}

	/**
	 * @return the tenant_password_suffix
	 */
	public String getTenant_password_suffix() {
		return tenant_password_suffix;
	}

	/**
	 * @return the status_message_success
	 */
	public String getStatus_message_success() {
		return status_message_success;
	}

	/**
	 * @return the status_message_failure
	 */
	public String getStatus_message_failure() {
		return status_message_failure;
	}

	/**
	 * @return the status_message_exception
	 */
	public String getStatus_message_exception() {
		return status_message_exception;
	}

	/**
	 * @return the email_regex
	 */
	public String getEmail_regex() {
		return email_regex;
	}

	/**
	 * @return the mobile_regex
	 */
	public String getMobile_regex() {
		return mobile_regex;
	}

	/**
	 * @return the password_regex
	 */
	public String getPassword_regex() {
		return password_regex;
	}

	/**
	 * @return the pincode_regex
	 */
	public String getPincode_regex() {
		return pincode_regex;
	}

	/**
	 * @return the name_regex
	 */
	public String getName_regex() {
		return name_regex;
	}

	/**
	 * @return the secret_key_salt_iterations
	 */
	public int getSecret_key_salt_iterations() {
		return secret_key_salt_iterations;
	}

	/**
	 * @return the secret_key_salt_algo
	 */
	public String getSecret_key_salt_algo() {
		return secret_key_salt_algo;
	}

	/**
	 * @return the secret_key_salt_algo_instance
	 */
	public String getSecret_key_salt_algo_instance() {
		return secret_key_salt_algo_instance;
	}

	/**
	 * @return the alpha_capital_case
	 */
	public String getAlpha_capital_case() {
		return alpha_capital_case;
	}

	/**
	 * @return the alpha_small_case
	 */
	public String getAlpha_small_case() {
		return alpha_small_case;
	}

	/**
	 * @return the special_chars
	 */
	public String getSpecial_chars() {
		return special_chars;
	}

	/**
	 * @return the natural_numbers
	 */
	public String getNatural_numbers() {
		return natural_numbers;
	}

	/**
	 * @return the status_response_type_code
	 */
	public String getStatus_response_type_code() {
		return status_response_type_code;
	}

	/**
	 * @return the status_response_type_token
	 */
	public String getStatus_response_type_token() {
		return status_response_type_token;
	}

	/**
	 * @return the status_response_type_password
	 */
	public String getStatus_response_type_password() {
		return status_response_type_password;
	}

	/**
	 * @return the secret_key_left_limit
	 */
	public int getSecret_key_left_limit() {
		return secret_key_left_limit;
	}

	/**
	 * @return the secret_key_right_limit
	 */
	public int getSecret_key_right_limit() {
		return secret_key_right_limit;
	}

	/**
	 * @return the secret_key_target_length
	 */
	public int getSecret_key_target_length() {
		return secret_key_target_length;
	}

	/**
	 * @return the auth_code_expiry_in_seconds
	 */
	public int getAuth_code_expiry_in_seconds() {
		return auth_code_expiry_in_seconds;
	}

	/**
	 * @return the auth_token_expiry_in_seconds
	 */
	public int getAuth_token_expiry_in_seconds() {
		return auth_token_expiry_in_seconds;
	}

	/**
	 * @return the service_type_save
	 */
	public int getService_type_save() {
		return service_type_save;
	}

	/**
	 * @return the service_type_update
	 */
	public int getService_type_update() {
		return service_type_update;
	}

	/**
	 * @return the admin_email_subject
	 */
	public String getAdmin_email_subject() {
		return admin_email_subject;
	}

	/**
	 * @return the tenant_email_subject
	 */
	public String getTenant_email_subject() {
		return tenant_email_subject;
	}

	/**
	 * @return the license_type_free
	 */
	public String getLicense_type_free() {
		return license_type_free;
	}

	/**
	 * @return the license_type_paid
	 */
	public String getLicense_type_paid() {
		return license_type_paid;
	}

	/**
	 * @return the license_type_free_days
	 */
	public int getLicense_type_free_days() {
		return license_type_free_days;
	}

	/**
	 * @return the license_type_paid_monthly
	 */
	public String getLicense_type_paid_monthly() {
		return license_type_paid_monthly;
	}

	/**
	 * @return the license_type_paid_quarterly
	 */
	public String getLicense_type_paid_quarterly() {
		return license_type_paid_quarterly;
	}

	/**
	 * @return the license_type_paid_annual
	 */
	public String getLicense_type_paid_annual() {
		return license_type_paid_annual;
	}

	/**
	 * @return the license_type_paid_monthly_days
	 */
	public int getLicense_type_paid_monthly_days() {
		return license_type_paid_monthly_days;
	}

	/**
	 * @return the license_type_paid_quarterly_days
	 */
	public int getLicense_type_paid_quarterly_days() {
		return license_type_paid_quarterly_days;
	}

	/**
	 * @return the license_type_paid_annual_days
	 */
	public int getLicense_type_paid_annual_days() {
		return license_type_paid_annual_days;
	}

	/**
	 * @return the end_date_key
	 */
	public String getEnd_date_key() {
		return end_date_key;
	}

	/**
	 * @return the license_days_key
	 */
	public String getLicense_days_key() {
		return license_days_key;
	}

	@Override
	public String toString() {
		return "SearchConstants [status_message_success=" + status_message_success + ", status_message_failure="
				+ status_message_failure + ", status_message_exception=" + status_message_exception + ", email_regex="
				+ email_regex + ", mobile_regex=" + mobile_regex + ", password_regex=" + password_regex
				+ ", pincode_regex=" + pincode_regex + ", name_regex=" + name_regex + ", secret_key_salt_iterations="
				+ secret_key_salt_iterations + ", secret_key_salt_algo=" + secret_key_salt_algo
				+ ", secret_key_salt_algo_instance=" + secret_key_salt_algo_instance + ", alpha_capital_case="
				+ alpha_capital_case + ", alpha_small_case=" + alpha_small_case + ", special_chars=" + special_chars
				+ ", natural_numbers=" + natural_numbers + ", status_response_type_code=" + status_response_type_code
				+ ", status_response_type_token=" + status_response_type_token + ", status_response_type_password="
				+ status_response_type_password + ", secret_key_left_limit=" + secret_key_left_limit
				+ ", secret_key_right_limit=" + secret_key_right_limit + ", secret_key_target_length="
				+ secret_key_target_length + ", auth_code_expiry_in_seconds=" + auth_code_expiry_in_seconds
				+ ", auth_token_expiry_in_seconds=" + auth_token_expiry_in_seconds + ", service_type_save="
				+ service_type_save + ", service_type_update=" + service_type_update + ", admin_email_subject="
				+ admin_email_subject + ", tenant_email_subject=" + tenant_email_subject + ", license_type_free="
				+ license_type_free + ", license_type_paid=" + license_type_paid + ", license_type_free_days="
				+ license_type_free_days + ", license_type_paid_monthly=" + license_type_paid_monthly
				+ ", license_type_paid_quarterly=" + license_type_paid_quarterly + ", license_type_paid_annual="
				+ license_type_paid_annual + ", license_type_paid_monthly_days=" + license_type_paid_monthly_days
				+ ", license_type_paid_quarterly_days=" + license_type_paid_quarterly_days
				+ ", license_type_paid_annual_days=" + license_type_paid_annual_days + ", end_date_key=" + end_date_key
				+ ", license_days_key=" + license_days_key + ", tenant_password_suffix=" + tenant_password_suffix
				+ ", db_url_prefix=" + db_url_prefix + ", db_host=" + db_host + ", db_port=" + db_port
				+ ", db_username=" + db_username + ", db_password=" + db_password + ", db_driver_class_name="
				+ db_driver_class_name + ", db_url_suffix=" + db_url_suffix + "]";
	}

}
