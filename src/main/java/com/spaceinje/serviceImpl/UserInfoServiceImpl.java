/**
 * 
 */
package com.spaceinje.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.DocumentException;
import com.spaceinje.bean.MailBean;
import com.spaceinje.bean.UserInfoBean;
import com.spaceinje.constants.MasterConstants;
import com.spaceinje.generator.EmailGenerator;
import com.spaceinje.generator.PDFGenerator;
import com.spaceinje.generator.SecretKeyGenerator;
import com.spaceinje.model.LicenseSpecifics;
import com.spaceinje.model.UserInfo;
import com.spaceinje.repo.LicenseSpecificsRepo;
import com.spaceinje.repo.UserInfoRepo;
import com.spaceinje.service.UserInfoService;
import com.spaceinje.util.ResponseCachet;

import brave.Tracer;
import freemarker.template.TemplateException;

/**
 * @author Lakshmi Kiran
 * @implNote service which describes the business for UserService
 * @version 1.0
 */
@Service("userInfoService")
public class UserInfoServiceImpl implements UserInfoService {

	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);

	/**
	 * admin user repository to make db transactions
	 */
	@Autowired
	private UserInfoRepo userInfoRepo;

	/**
	 * Model mapper to map entity to dto and dto to entity
	 */
	@Autowired
	private ModelMapper modelMapper;

	/**
	 * email generator which generates the email and sends to registered email
	 */
	@Autowired
	private EmailGenerator emailGenerator;

	@Autowired
	Tracer tracer;

	@Autowired
	private MasterConstants constants;

	@Autowired
	private SecretKeyGenerator secretKeyGenerator;

	@Autowired
	private PDFGenerator pdfGenerator;

	@Autowired
	private LicenseSpecificsRepo licenseSpecificsRepo;

	/**
	 * service to save user
	 * 
	 * @implNote while saving user, a pdf file will be generated with admin hash and
	 *           secret and will be sent to mail
	 * 
	 * @exception IllegalArgumentException, NoSuchAlgorithmException,
	 *                                      InvalidKeySpecException,
	 *                                      DocumentException, IOException,
	 *                                      TemplateException, AddressException,
	 *                                      MessagingException
	 * @return {@value ResponseCachet}
	 */
	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = { IllegalArgumentException.class, NoSuchAlgorithmException.class,
			InvalidKeySpecException.class, DocumentException.class, IOException.class, TemplateException.class,
			AddressException.class, MessagingException.class })
	@Override
	public ResponseCachet<UserInfoBean> saveUserInfo(UserInfoBean bean) {
		if (log.isDebugEnabled())
			log.debug("Entering saveUserInfo service");
		ResponseCachet<UserInfoBean> cachet = new ResponseCachet<UserInfoBean>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			UserInfo user = userInfoRepo.findByEmailOrMobile(bean.getEmail(), bean.getMobile(), bean.isIs_tenant());
			if (null != user) {
				log.error("user already exists with email or mobile");
				cachet.setMessage("user already exists with email or mobile");
				cachet.setStatus(constants.getStatus_message_failure());
				return cachet;
			}
			log.info("converting bean to entity");
			user = modelMapper.map(bean, UserInfo.class);
			log.info("generating password hash");
			String new_password = secretKeyGenerator.generatePassword() + constants.getPassword_suffix();
			String password = secretKeyGenerator.generateHash(constants.getStatus_response_type_password(),
					new_password);
			user.setPassword(password);
			user.setLogin_attempts(0);
			user.setLogin_status(constants.getLogin_status_new());
			// setting admin or tenant unique code
			String user_code;
			if (bean.isIs_tenant()) {
				user_code = constants.getTenant_id_prefix() + RandomStringUtils.randomNumeric(constants.getId_length());
				user.setTenant_code(user_code);
			} else {
				user_code = constants.getAdmin_id_prefix() + RandomStringUtils.randomNumeric(constants.getId_length());
				user.setAdmin_code(user_code);
			}

			log.info("saving user secret info");
			String hash = secretKeyGenerator.generateHash(constants.getStatus_response_type_code(),
					secretKeyGenerator.generatePassword());
			String secret = secretKeyGenerator.generateHash(constants.getStatus_response_type_token(),
					secretKeyGenerator.generatePassword());
			ResponseCachet res = null;
			String filePass = user.getFirst_name().toLowerCase().substring(0, 4)
					+ user.getMobile().substring(user.getMobile().length() - 4, user.getMobile().length());
			try {
				String filepath = pdfGenerator.createPDF(hash, secret, user.getFirst_name().replace(" ", ""), user_code,
						filePass);

				log.info("preparing the mail bean props to send mail to admin");
				MailBean mail = new MailBean();
				mail.setFilepath(filepath);
				mail.setTo(user.getEmail());
				mail.setUsername(user.getFirst_name());
				mail.setHas_attachement(Boolean.TRUE);
				mail.setFile_name("JE_" + filepath.split("JE_")[1]);
				mail.setMedia_type(MediaType.APPLICATION_PDF_VALUE);
				Map<String, Object> model = new HashMap<String, Object>();
				model.put("username", mail.getUsername());
				model.put("password", new_password);
				model.put("code", user_code);
				if (bean.isIs_tenant()) {
					mail.setSubject(constants.getTenant_email_subject());
					mail.setTemplate_name("tenant_registration.html");
					model.put("license_start_date", bean.getLicense().getLicense_start_date().toString());
					model.put("license_end_date", bean.getLicense().getLicense_end_date().toString());
					model.put("license_active_days", bean.getLicense().getLicense_active_days());
					model.put("license_type", bean.getLicense().getLicense_type());
				} else {
					mail.setSubject(constants.getAdmin_email_subject());
					mail.setTemplate_name("admin_registration.html");
				}
				mail.setModel(model);

				log.info("calling email generator service");
				res = emailGenerator.sendMail(mail);

				// deleting the generated pdf files
				File file = new File(filepath);
				boolean delete = file.delete();
				filepath = filepath.replace("JE_", "");
				File enc_file = new File(filepath);
				boolean enc_delete = enc_file.delete();
				if (delete && enc_delete)
					log.info("pdf files deleted successfully");

				if (res.getStatus().equals(constants.getStatus_message_failure())) {
					cachet.setMessage("exception while sending email");
					cachet.setStatus(constants.getStatus_message_failure());
					return cachet;
				}

			} catch (DocumentException | IOException e) {
				log.error("DocumentException :: IOException :: exception while creating pdf", e);
				cachet.setMessage(" exception while creating pdf");
				cachet.setStatus(constants.getStatus_message_failure());
				return cachet;
			}

			user.setHash(hash);
			user.setSecret(secret);

			LicenseSpecifics specifics = null;
			if (bean.isIs_tenant()) {
				specifics = bean.getLicense();
				specifics.setTenant_code(user_code);
				log.info("saving tenant license specifics");
				specifics = licenseSpecificsRepo.save(specifics);
			}

			log.info("saving user info");
			user = userInfoRepo.save(user);

			// mapping entity to bean
			bean = new UserInfoBean();
			bean = modelMapper.map(user, UserInfoBean.class);
			if (null != specifics)
				bean.setLicense(specifics);
			cachet.setMessage("user saved successfully");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setData(bean);

		} catch (IllegalArgumentException e) {
			log.error("IllegalArgumentException :: entity can't be null", e);
			cachet.setMessage("exception while saving user");
			cachet.setStatus(constants.getStatus_message_exception());
		} catch (NoSuchAlgorithmException e) {
			log.error("NoSuchAlgorithmException :: exception while generating password hash", e);
			cachet.setMessage("exception while saving user");
			cachet.setStatus(constants.getStatus_message_exception());
		} catch (InvalidKeySpecException e) {
			log.error("InvalidKeySpecException :: exception while generating password hash", e);
			cachet.setMessage("exception while saving user");
			cachet.setStatus(constants.getStatus_message_exception());
		}
		if (log.isDebugEnabled())
			log.debug("Exiting saveUserInfo service");
		return cachet;
	}

	/**
	 * service to update user
	 * 
	 * @return {@value ResponseCachet}
	 */
	@Transactional(rollbackFor = { IllegalArgumentException.class })
	@Override
	public ResponseCachet<UserInfoBean> updateUserInfo(UserInfoBean bean) {
		if (log.isDebugEnabled())
			log.debug("Entering updateAdminUser service");
		ResponseCachet<UserInfoBean> cachet = new ResponseCachet<UserInfoBean>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			UserInfo user = userInfoRepo.findById(bean.getId());
			if (null == user) {
				log.error("user doesnot exists with id : " + bean.getId());
				cachet.setMessage("user doesnot exists with id : " + bean.getId());
				cachet.setStatus(constants.getStatus_message_failure());
				return cachet;
			}
			log.info("converting bean to entity");
			user = modelMapper.map(bean, UserInfo.class);
			log.info("updating user");
			user = userInfoRepo.save(user);
			log.info("user updated successfully" + user);
			cachet.setMessage("user updated successfully");
			cachet.setStatus(constants.getStatus_message_success());
			bean = modelMapper.map(user, UserInfoBean.class);
			cachet.setData(bean);
		} catch (IllegalArgumentException e) {
			log.error("IllegalArgumentException :: entity can't be null", e);
			cachet.setMessage("exception while updating admin");
			cachet.setStatus(constants.getStatus_message_exception());
		}
		if (log.isDebugEnabled())
			log.debug("Exiting updateUserInfo service");
		return cachet;
	}

	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = { IllegalArgumentException.class })
	@Override
	public ResponseCachet deleteUserInfo(String id) {
		if (log.isDebugEnabled())
			log.debug("Entering deleteUserInfo service");
		ResponseCachet<UserInfoBean> cachet = new ResponseCachet<UserInfoBean>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			UserInfo user = userInfoRepo.findById(id);
			if (null == user) {
				log.error("user doesnot exists with id : " + id);
				cachet.setMessage("user doesnot exists with id : " + id);
				cachet.setStatus(constants.getStatus_message_failure());
				return cachet;
			}
			log.info("removing user");
			userInfoRepo.delete(user);
			log.info("user deleted successfully");

			cachet.setMessage("user deleted successfully");
			cachet.setStatus(constants.getStatus_message_success());
		} catch (IllegalArgumentException e) {
			log.error("IllegalArgumentException :: entity can't be null", e);
			cachet.setMessage("exception while deleting admin");
			cachet.setStatus(constants.getStatus_message_exception());
		}
		if (log.isDebugEnabled())
			log.debug("Exiting deleteUserInfo service");
		return cachet;
	}

}
