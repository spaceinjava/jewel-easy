/**
 * 
 */
package com.spaceinje.validator;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.spaceinje.bean.UserInfoBean;
import com.spaceinje.constants.MasterConstants;
import com.spaceinje.util.ResponseCachet;
import com.spaceinje.util.SigninUtil;

import brave.Tracer;

/**
 * @author Lakshmi Kiran
 * @implNote class which validate all the parameters of admin user request
 * @version 1.0
 */
@Component("requestValidator")
@Singleton
public class RequestValidator {

	private static final Logger log = LoggerFactory.getLogger(RequestValidator.class);

	@Autowired
	private MasterConstants constants;

	@Autowired
	private BasicValidator basicValidator;

	@Autowired
	Tracer tracer;

	/**
	 * service to validate params of user
	 * 
	 * @param bean        -UserInfoBean
	 * 
	 * @param serviceType - either save or update
	 * 
	 * @return {@value ResponseCachet}
	 */
	@SuppressWarnings("rawtypes")
	public ResponseCachet isUserRequestValid(UserInfoBean bean, int serviceType) {
		if (log.isDebugEnabled())
			log.debug("Entering isAdminBeanValid service");
		ResponseCachet cachet = new ResponseCachet();
		if (null == bean) {
			log.info("request can't be empty");
			cachet.setMessage("request can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			if (log.isDebugEnabled())
				log.debug("Exiting isUserRequestValid service");
			return cachet;
		}
		if (serviceType == constants.getService_type_update()) {
			if (StringUtils.isEmpty(bean.getId())) {
				log.info("id can't be empty or invalid");
				cachet.setMessage("id can't be empty or invalid");
				cachet.setStatus(constants.getStatus_message_failure());
				if (log.isDebugEnabled())
					log.debug("Exiting isUserRequestValid service");
				return cachet;
			}
		}
		if (StringUtils.isEmpty(bean.getEmail())) {
			log.info("email can't be empty");
			cachet.setMessage("email can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			if (log.isDebugEnabled())
				log.debug("Exiting isUserRequestValid service");
			return cachet;
		} else {
			if (!basicValidator.isValidEmail(bean.getEmail())) {
				log.info("invalid email");
				cachet.setMessage("invalid email");
				cachet.setStatus(constants.getStatus_message_failure());
				if (log.isDebugEnabled())
					log.debug("Exiting isUserRequestValid service");
				return cachet;
			}
		}

		if (StringUtils.isEmpty(bean.getMobile())) {
			log.info("mobile can't be empty");
			cachet.setMessage("mobile can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			if (log.isDebugEnabled())
				log.debug("Exiting isUserRequestValid service");
			return cachet;
		} else {
			if (!basicValidator.isMobileValid(bean.getMobile())) {
				log.info("invalid mobile");
				cachet.setMessage(
						"invalid mobile : The first digit should contain number between 6 to 9. The rest 9\r\n"
								+ "	 *           digit can contain any number between 0 to 9. The mobile number can\r\n"
								+ "	 *           have 11 digits also by including 0 at the starting. The mobile\r\n"
								+ "	 *           number can be of 12 digits also by including 91 at the starting");
				cachet.setStatus(constants.getStatus_message_failure());
				if (log.isDebugEnabled())
					log.debug("Exiting isUserRequestValid service");
				return cachet;
			}
		}

		if (StringUtils.isEmpty(bean.getFirst_name())) {
			log.info("first name can't be empty");
			cachet.setMessage("first name can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			if (log.isDebugEnabled())
				log.debug("Exiting isUserRequestValid service");
			return cachet;
		} else {
			if (!basicValidator.isNameValid(bean.getFirst_name())) {
				log.info("invalid first name");
				cachet.setMessage("invalid first name : ignore case: \"(?i)\"\r\n"
						+ "can only start with an a-z character\r\n" + "can only end with an a-z\r\n"
						+ "is between 1 and 25 characters\r\n" + "can contain a-z and [ '-,.]\"");
				cachet.setStatus(constants.getStatus_message_failure());
				if (log.isDebugEnabled())
					log.debug("Exiting isUserRequestValid service");
				return cachet;
			}
		}
		if (StringUtils.isEmpty(bean.getLast_name())) {
			log.info("last name can't be empty");
			cachet.setMessage("last name can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			if (log.isDebugEnabled())
				log.debug("Exiting isUserRequestValid service");
			return cachet;
		} else {
			if (!basicValidator.isNameValid(bean.getLast_name())) {
				log.info("invalid last name");
				cachet.setMessage("invalid first name : ignore case: \"(?i)\"\r\n"
						+ "can only start with an a-z character\r\n" + "can only end with an a-z\r\n"
						+ "is between 1 and 25 characters\r\n" + "can contain a-z and [ '-,.]\"");
				cachet.setStatus(constants.getStatus_message_failure());
				if (log.isDebugEnabled())
					log.debug("Exiting isUserRequestValid service");
				return cachet;
			}
		}
		if (!StringUtils.isEmpty(bean.getMiddle_name())) {
			if (!basicValidator.isNameValid(bean.getMiddle_name())) {
				log.info("invalid middle name");
				cachet.setMessage("invalid middle name : ignore case: \"(?i)\"\r\n"
						+ "can only start with an a-z character\r\n" + "can only end with an a-z\r\n"
						+ "is between 1 and 25 characters\r\n" + "can contain a-z and [ '-,.]\"");
				cachet.setStatus(constants.getStatus_message_failure());
				if (log.isDebugEnabled())
					log.debug("Exiting isAdminBeanValid service");
				return cachet;
			}
		}
		cachet.setMessage("Requested user is valid");
		cachet.setStatus(constants.getStatus_message_success());
		if (log.isDebugEnabled())
			log.debug("Exiting isUserRequestValid service");
		return cachet;
	}
	
	/**
	 * method to validate user sign in utils
	 * 
	 * @param util - SigninUtil
	 * 
	 * @return {@value ResponseEntity<ResponseCachet>}
	 */
	@SuppressWarnings("rawtypes")
	public ResponseEntity<ResponseCachet> validateSignInUtils(SigninUtil util, int request_type) {
		if (log.isDebugEnabled()) {
			log.debug("Entering validateSignInUtils method");
		}
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (null == util) {
			log.info("request can't be null");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("request can't be null");
			if (log.isDebugEnabled())
				log.debug("Exiting validateSignInUtils method");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (StringUtils.isEmpty(util.getUser_code())) {
			log.info("user name can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("user name can't be empty");
			if (log.isDebugEnabled())
				log.debug("Exiting validateSignInUtils method");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (StringUtils.isEmpty(util.getPassword())) {
			log.info("password can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("password can't be empty");
			if (log.isDebugEnabled())
				log.debug("Exiting validateSignInUtils method");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (request_type == constants.getService_type_update()) {
			if (StringUtils.isEmpty(util.getNew_password())) {
				log.info("new password can't be empty");
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("new password can't be empty");
				if (log.isDebugEnabled())
					log.debug("Exiting validateSignInUtils method");
				return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
			}
		}
		cachet.setStatus(constants.getStatus_message_success());
		cachet.setMessage("signin utils validated successfully");
		if (log.isDebugEnabled())
			log.debug("Exiting validateSignInUtils method");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
