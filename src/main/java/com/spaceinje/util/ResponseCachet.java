/**
 * 
 */
package com.spaceinje.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Lakshmi Kiran
 * 
 * @implSpec status which contains authorization and authentication tokens along
 *           with their expire time and roles
 *
 * @version 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseCachet<T> {

	@JsonInclude(Include.NON_NULL)
	private String status;

	@JsonInclude(Include.NON_NULL)
	private String message;

	@JsonInclude(Include.NON_NULL)
	private String traceId;

	@JsonInclude(Include.NON_NULL)
	private T data;

	@JsonInclude(Include.NON_NULL)
	private UserCachet info;

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the traceId
	 */
	public String getTraceId() {
		return traceId;
	}

	/**
	 * @param traceId the traceId to set
	 */
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the info
	 */
	public UserCachet getInfo() {
		return info;
	}

	/**
	 * @param info the info to set
	 */
	public void setInfo(UserCachet info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return "ResponseCachet [status=" + status + ", message=" + message + ", traceId=" + traceId + ", data=" + data
				+ ", info=" + info + "]";
	}

}
