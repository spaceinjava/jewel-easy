/**
 * 
 */
package com.spaceinje.util;

/**
 * @author Lakshmi Kiran
 * @implNote used as login request for tenant and employee
 * @version 1.0
 */
public class SigninUtil {

	private String user_code;
	
	private String password;
	
	private String new_password;
	

	/**
	 * @return the new_password
	 */
	public String getNew_password() {
		return new_password;
	}

	/**
	 * @param new_password the new_password to set
	 */
	public void setNew_password(String new_password) {
		this.new_password = new_password;
	}

	/**
	 * @return the user_name
	 */
	public String getUser_code() {
		return user_code;
	}

	/**
	 * @param user_name the user_name to set
	 */
	public void setUser_code(String user_name) {
		this.user_code = user_name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "SigninUtil [user_name=" + user_code + ", password=" + password + ", new_password=" + new_password + "]";
	}
	
	
}
