/**
 * 
 */
package com.spaceinje.bean.master;

import java.util.Date;

/**
 * @author Lakshmi Kiran
 *
 */
public class AdminTypeBean {
	
	private Integer admin_type_id;

	/**
	 * Type of admin {@value SUPER_ADMIN, ADMIN, DEV, QA}, values are tentative
	 */
	private String admin_type;

	/**
	 * Date of creation of ADMIN_TYPE
	 */
	private Date created_date;

	/**
	 * Date of modification of ADMIN_TYPE
	 */
	private Date modified_date;

	/**
	 * @return the admin_type_id
	 */
	public Integer getAdmin_type_id() {
		return admin_type_id;
	}

	/**
	 * @param admin_type_id the admin_type_id to set
	 */
	public void setAdmin_type_id(Integer admin_type_id) {
		this.admin_type_id = admin_type_id;
	}

	/**
	 * @return the admin_type
	 */
	public String getAdmin_type() {
		return admin_type;
	}

	/**
	 * @param admin_type the admin_type to set
	 */
	public void setAdmin_type(String admin_type) {
		this.admin_type = admin_type;
	}

	/**
	 * @return the created_date
	 */
	public Date getCreated_date() {
		return created_date;
	}

	/**
	 * @param created_date the created_date to set
	 */
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	/**
	 * @return the modified_date
	 */
	public Date getModified_date() {
		return modified_date;
	}

	/**
	 * @param modified_date the modified_date to set
	 */
	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}

	@Override
	public String toString() {
		return "AdminTypeBean [admin_type_id=" + admin_type_id + ", admin_type=" + admin_type + ", created_date="
				+ created_date + ", modified_date=" + modified_date + "]";
	}
	
	
}
