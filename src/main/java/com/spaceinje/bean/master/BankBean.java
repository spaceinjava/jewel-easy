package com.spaceinje.bean.master;

/**
 *
 * @author Manikanta
 */
public class BankBean {

    private Integer bankId;
    private String bank;

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    
}
