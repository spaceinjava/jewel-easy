package com.spaceinje.bean.master;

/**
 *
 * @author Narayana
 */
public class DistrictBean {

    private Integer districtId;
    private String district;
    private StateBean state;

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public StateBean getState() {
        return state;
    }

    public void setState(StateBean state) {
        this.state = state;
    }

}
