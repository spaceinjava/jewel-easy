package com.spaceinje.bean.master;

/**
 *
 * @author Narayana
 */
public class MainGroupBean {

    private Integer mainGroupId;
    private Integer mainGroupName;

    public Integer getMainGroupId() {
        return mainGroupId;
    }

    public void setMainGroupId(Integer mainGroupId) {
        this.mainGroupId = mainGroupId;
    }

    public Integer getMainGroupName() {
        return mainGroupName;
    }

    public void setMainGroupName(Integer mainGroupName) {
        this.mainGroupName = mainGroupName;
    }

}
