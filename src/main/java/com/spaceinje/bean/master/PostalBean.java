package com.spaceinje.bean.master;

/**
 *
 * @author Narayana
 */
public class PostalBean {

    private Integer postalId;
    private String postalName;
    private String pincode;
    private SubDistrictBean subDistrict;
    private DistrictBean district;
    private StateBean state;
    private CountryBean country;

    public Integer getPostalId() {
        return postalId;
    }

    public void setPostalId(Integer postalId) {
        this.postalId = postalId;
    }

    public String getPostalName() {
        return postalName;
    }

    public void setPostalName(String postalName) {
        this.postalName = postalName;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public SubDistrictBean getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(SubDistrictBean subDistrict) {
        this.subDistrict = subDistrict;
    }

    public DistrictBean getDistrict() {
        return district;
    }

    public void setDistrict(DistrictBean district) {
        this.district = district;
    }

    public StateBean getState() {
        return state;
    }

    public void setState(StateBean state) {
        this.state = state;
    }

    public CountryBean getCountry() {
        return country;
    }

    public void setCountry(CountryBean country) {
        this.country = country;
    }

}
