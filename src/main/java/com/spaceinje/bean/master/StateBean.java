package com.spaceinje.bean.master;

/**
 *
 * @author Narayana
 */
public class StateBean {

    private Integer stateId;
    private String state;
    private CountryBean country;

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public CountryBean getCountry() {
        return country;
    }

    public void setCountry(CountryBean country) {
        this.country = country;
    }

}
