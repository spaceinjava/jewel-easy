package com.spaceinje.bean.master;

/**
 *
 * @author Narayana
 */
public class CountryBean {

    private Integer countryId;
    private String country;

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
