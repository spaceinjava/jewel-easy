package com.spaceinje.bean.master;

/**
 *
 * @author Narayana
 */
public class SubDistrictBean {

    private Integer subDistrictId;
    private String subDistrict;
    private String subDistrictCode;
    private DistrictBean district;

    public Integer getSubDistrictId() {
        return subDistrictId;
    }

    public void setSubDistrictId(Integer subDistrictId) {
        this.subDistrictId = subDistrictId;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getSubDistrictCode() {
        return subDistrictCode;
    }

    public void setSubDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
    }

    public DistrictBean getDistrict() {
        return district;
    }

    public void setDistrict(DistrictBean district) {
        this.district = district;
    }

}
