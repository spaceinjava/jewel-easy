package com.spaceinje.bean.master;

/**
 *
 * @author Narayana
 */
public class MetalTypeBean {

    private Integer metalTypeId;
    private String metalType;

    public Integer getMetalTypeId() {
        return metalTypeId;
    }

    public void setMetalTypeId(Integer metalTypeId) {
        this.metalTypeId = metalTypeId;
    }

    public String getMetalType() {
        return metalType;
    }

    public void setMetalType(String metalType) {
        this.metalType = metalType;
    }

}
