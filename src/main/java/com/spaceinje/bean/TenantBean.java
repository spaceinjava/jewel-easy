package com.spaceinje.bean;

import java.util.Arrays;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TenantBean {

	private Integer tenant_id;

	private String first_name;

	private String middle_name;

	private String last_name;

	private String email;

	private String mobile;

	private String gender;

	private String profile_picture;

	private String employee_id;
	
	private String[] roles;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date date_of_birth;

	private Integer tenant_type;

	private Integer login_attempts;

	private String login_status;

	private Boolean is_tenant_license_free;

	private String license_paid_type;

	private int license_expiry_days;

	private Date license_start_date;

	private Date license_end_date;

	private Date created_date;

	private Date modified_date;
	
	private String created_by;
	
	private String updated_by;
	
	
	
	/**
	 * @return the roles
	 */
	public String[] getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	/**
	 * @return the created_by
	 */
	public String getCreated_by() {
		return created_by;
	}

	/**
	 * @param created_by the created_by to set
	 */
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	/**
	 * @return the updated_by
	 */
	public String getUpdated_by() {
		return updated_by;
	}

	/**
	 * @param updated_by the updated_by to set
	 */
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public String getLicense_paid_type() {
		return license_paid_type;
	}

	public void setLicense_paid_type(String license_paid_type) {
		this.license_paid_type = license_paid_type;
	}

	/**
	 * @return the tenant_id
	 */
	public Integer getTenant_id() {
		return tenant_id;
	}

	/**
	 * @param tenant_id the tenant_id to set
	 */
	public void setTenant_id(Integer tenant_id) {
		this.tenant_id = tenant_id;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	/**
	 * @return the middle_name
	 */
	public String getMiddle_name() {
		return middle_name;
	}

	/**
	 * @param middle_name the middle_name to set
	 */
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the profile_picture
	 */
	public String getProfile_picture() {
		return profile_picture;
	}

	/**
	 * @param profile_picture the profile_picture to set
	 */
	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	/**
	 * @return the employee_id
	 */
	public String getEmployee_id() {
		return employee_id;
	}

	/**
	 * @param employee_id the employee_id to set
	 */
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	/**
	 * @return the date_of_birth
	 */
	public Date getDate_of_birth() {
		return date_of_birth;
	}

	/**
	 * @param date_of_birth the date_of_birth to set
	 */
	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	/**
	 * @return the tenant_type
	 */
	public Integer getTenant_type() {
		return tenant_type;
	}

	/**
	 * @param tenant_type the tenant_type to set
	 */
	public void setTenant_type(Integer tenant_type) {
		this.tenant_type = tenant_type;
	}

	/**
	 * @return the created_date
	 */
	public Date getCreated_date() {
		return created_date;
	}

	/**
	 * @param created_date the created_date to set
	 */
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	/**
	 * @return the modified_date
	 */
	public Date getModified_date() {
		return modified_date;
	}

	/**
	 * @param modified_date the modified_date to set
	 */
	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}

	/**
	 * @return the login_attempts
	 */
	public Integer getLogin_attempts() {
		return login_attempts;
	}

	/**
	 * @param login_attempts the login_attempts to set
	 */
	public void setLogin_attempts(Integer login_attempts) {
		this.login_attempts = login_attempts;
	}

	/**
	 * @return the login_status
	 */
	public String getLogin_status() {
		return login_status;
	}

	/**
	 * @param login_status the login_status to set
	 */
	public void setLogin_status(String login_status) {
		this.login_status = login_status;
	}

	/**
	 * @return the is_tenant_license_free
	 */
	public Boolean getIs_tenant_license_free() {
		return is_tenant_license_free;
	}

	/**
	 * @param is_tenant_license_free the is_tenant_license_free to set
	 */
	public void setIs_tenant_license_free(Boolean is_tenant_license_free) {
		this.is_tenant_license_free = is_tenant_license_free;
	}

	/**
	 * @return the license_expiry_days
	 */
	public int getLicense_expiry_days() {
		return license_expiry_days;
	}

	/**
	 * @param license_expiry_days the license_expiry_days to set
	 */
	public void setLicense_expiry_days(int license_expiry_days) {
		this.license_expiry_days = license_expiry_days;
	}

	/**
	 * @return the license_start_date
	 */
	public Date getLicense_start_date() {
		return license_start_date;
	}

	/**
	 * @param license_start_date the license_start_date to set
	 */
	public void setLicense_start_date(Date license_start_date) {
		this.license_start_date = license_start_date;
	}

	/**
	 * @return the license_end_date
	 */
	public Date getLicense_end_date() {
		return license_end_date;
	}

	/**
	 * @param license_end_date the license_end_date to set
	 */
	public void setLicense_end_date(Date license_end_date) {
		this.license_end_date = license_end_date;
	}

	@Override
	public String toString() {
		return "TenantBean [tenant_id=" + tenant_id + ", first_name=" + first_name + ", middle_name=" + middle_name
				+ ", last_name=" + last_name + ", email=" + email + ", mobile=" + mobile + ", gender=" + gender
				+ ", profile_picture=" + profile_picture + ", employee_id=" + employee_id + ", roles="
				+ Arrays.toString(roles) + ", date_of_birth=" + date_of_birth + ", tenant_type=" + tenant_type
				+ ", login_attempts=" + login_attempts + ", login_status=" + login_status + ", is_tenant_license_free="
				+ is_tenant_license_free + ", license_paid_type=" + license_paid_type + ", license_expiry_days="
				+ license_expiry_days + ", license_start_date=" + license_start_date + ", license_end_date="
				+ license_end_date + ", created_date=" + created_date + ", modified_date=" + modified_date
				+ ", created_by=" + created_by + ", updated_by=" + updated_by + "]";
	}
}
