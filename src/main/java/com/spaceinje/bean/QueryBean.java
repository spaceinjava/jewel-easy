/**
 * 
 */
package com.spaceinje.bean;

/**
 * @author Lakshmi Kiran
 * @implNote used as a request for executing database queries
 * @version 1.0
 */
public class QueryBean {

	private String query;
	
	private String db_name;

	/**
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * @return the db_name
	 */
	public String getDb_name() {
		return db_name;
	}

	/**
	 * @param db_name the db_name to set
	 */
	public void setDb_name(String db_name) {
		this.db_name = db_name;
	}

	@Override
	public String toString() {
		return "QueryBean [query=" + query + ", db_name=" + db_name + "]";
	}
	
	
}
