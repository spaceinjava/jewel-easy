package com.spaceinje;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.client.RestTemplate;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@RefreshScope
@EnableEurekaServer
@EnableCircuitBreaker
@EnableHystrixDashboard
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Jewel Easy Master Service", description = "contains the services related to admin", version = "1.0"))
@EnableMongoRepositories(basePackages = { "com.spaceinje.repo.nosql" })
@EnableMongoAuditing
@EnableCaching
@EnableWebSecurity
public class JewelEasyApplication {

	public static void main(String[] args) {
		SpringApplication.run(JewelEasyApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
}
