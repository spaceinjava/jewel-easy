FROM java:8
RUN mkdir -p /opt/pdf-files
RUN chmod -R 7777 /opt/pdf-files
WORKDIR /
ADD target/jewel-easy-*.jar jewel-easy-master.jar
EXPOSE 80
CMD ["java", "-jar", "jewel-easy-master.jar"]
